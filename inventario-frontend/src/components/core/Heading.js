import React, { Component } from 'react'

class Heading extends Component {
    render() {
        const props = this.props;
        const Element = 'h' + props.level;

        return (
            <Element className={props.className}>
                {props.children}
            </Element>
        );
    }
}

export default Heading;