import { Button, Col, Container, Form, Row, FormGroup, Input, FormFeedback } from 'reactstrap';
import Heading from '../core/Heading'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux'
import { registerUser } from '../../actions/authActions'

class Register extends Component {

    constructor() {
        super();
        this.state = {
            name: '',
            email: '',
            password: '',
            password2: '',
            errors: {}
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        if(this.props.auth.isAuthenticated) {
            this.props.history.push('/dashboard')
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.errors) {
            this.setState({errors: nextProps.errors})
        }
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    onSubmit(e) {
        e.preventDefault();

        const newUser = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            password2: this.state.password2,
        };

        this.props.registerUser(newUser, this.props.history);
    }

    render() {
        return (
            <Container className="register">
                {this.renderRow()}
            </Container>
        )
    }

    renderRow() {
        return  (
            <Row>
                <Col md={{ size: 8, offset: 2 }}>
                    {this.renderHeading()}
                    {this.renderSubHeading()}
                    {this.renderForm()}
                </Col>
            </Row>
        );
    }

    renderHeading() {
        const headingProps = {
            className: 'display-4 text-center',
            level: 2
        }

        return (
            <Heading {...headingProps}>
                Registrate!
            </Heading>
        );
    }

    renderSubHeading() {
        return (
            <p className="lead text-center">
                Crea tu cuenta para AptConnector
            </p>
        )
    }

    renderForm() {
        return (
            <Form onSubmit={this.onSubmit}>
                {this.renderFormGroup('name')}
                {this.renderFormGroup('email')}
                {this.renderFormGroup('password')}
                {this.renderFormGroup('password2')}
                {this.renderSubmitButton()}
            </Form>
        );
    }

    renderFormGroup(type) {
        const formControlProps = this.getFormGroupProps(type);
        const formFeedbackProps = this.getFormFeedbackProps(type);

        return (
            <FormGroup>
                <Input {...formControlProps}/>
                <FormFeedback {...formFeedbackProps}> {this.state.errors[type]} </FormFeedback>
            </FormGroup>
        )
    }

    renderSubmitButton() {
        const submitProps = {
            color: 'info',
            block: true
        };

        return (
            <Button {...submitProps}>Registrarse</Button> 
        );  
    }

    getFormGroupProps(type) {
        let formControlProps;
        let formControlPropsType = {
            name: this.getNameProps(),
            email: this.getEmailProps(),
            password: this.getPasswordProps(0),
            password2: this.getPasswordProps(1)
        }

        formControlProps = formControlPropsType[type];
        formControlProps.onChange = this.onChange;

        return formControlProps;
    }

    getFormFeedbackProps(type) {
        let formControlProps;
        let formControlPropsType = {
            type: this.isInvalidForErrors(type)
        }

        formControlProps = formControlPropsType[type]

        return formControlProps;
    }

    isInvalidForErrors(input) {
        return {
            invalid : this.state.errors[input] === undefined ? undefined: true
        }
    }

    getNameProps() {
        let state = this.state;

        return {
            type: 'text',
            placeholder: 'Nombre',
            invalid : state.errors.name,
            name: 'name',
            value: state.name
        }
    }

    getEmailProps() {
        let state = this.state;
        
        return {
            type: 'email',
            placeholder: 'Email',
            invalid : state.errors.email,
            name: 'email',
            value: state.email
        };
    }

    getErrors() {
        return this.state.errors;
    }

    getPasswordProps(option) {
        let state = this.state;

        return {
            type: 'password',
            placeholder: option === 0 ? 'Contraseña' : 'Confirma Contraseña',
            name: option === 0 ? 'password' : 'password2',
            value: option === 0 ? state.password : state.password2,
            invalid : option === 0 ? state.errors.password : state.errors.password2,
        };
    }
}

Register.propTypes = {
    registerUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired, 
    errors: PropTypes.object.isRequired
}
 
const mapStateToProps = (state) => ({
    auth: state.auth,
    errors: state.errors
})

export default connect(mapStateToProps, { registerUser })(withRouter(Register));