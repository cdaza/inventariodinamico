import { Button, Col, Container, Form, Row, FormGroup, Input, FormFeedback } from 'reactstrap';
import Heading from '../core/Heading'
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginUser } from '../../actions/authActions'

class Login extends Component {

    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            errors: {}
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        if(this.props.auth.isAuthenticated) {
            this.props.history.push('/dashboard')
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.auth.isAuthenticated) {
            this.props.history.push('/dashboard');
        }

        if(nextProps.errors) {
            this.setState({errors: nextProps.errors})
        }
    }


    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();

        const userData = {
            email: this.state.email,
            password: this.state.password
        }

        this.props.loginUser(userData)
    }

    render() {
        return (
            <Container>
                {this.renderRow()}
            </Container>
        )
    }

    renderRow() {
        return (
            <Row>
                <Col md={{ size: 8, offset: 2 }}>
                    {this.renderHeading()}
                    {this.renderSubHeading()}
                    {this.renderForm()}
                </Col>
            </Row>
        );
    }

    renderHeading() {
        const headingProps = {
            className: 'display-4 text-center',
            level: 2
        }

        return (
            <Heading {...headingProps}>
                Iniciar Sesión!
            </Heading>
        );
    }

    renderSubHeading() {
        return (
            <p className="lead text-center">
                Inicia sesión con tu cuenta de AptConnector
            </p>
        )
    }

    renderForm() {
        return (
            <Form onSubmit={this.onSubmit}>
                {this.renderFormGroup('email')}
                {this.renderFormGroup('password')}
                {this.renderSubmitButton()}
            </Form>
        );
    }

    renderFormGroup(type) {
        const formControlProps = this.getFormGroupProps(type);
        const formFeedbackProps = this.getFormFeedbackProps(type);

        return (
            <FormGroup>
                <Input {...formControlProps}/>
                <FormFeedback {...formFeedbackProps}> {this.state.errors[type]} </FormFeedback>
            </FormGroup>
        )
    }

    getFormGroupProps(type) {
        let formControlProps;
        let formControlPropsType = {
            email: this.getEmailProps(),
            password: this.getPasswordProps()
        }

        formControlProps = formControlPropsType[type];
        formControlProps.onChange = this.onChange;

        return formControlProps;
    }

    getFormFeedbackProps(type) {
        let formControlProps;
        let formControlPropsType = {
            type: this.isInvalidForErrors(type)
        }

        formControlProps = formControlPropsType[type]

        return formControlProps;
    }

    isInvalidForErrors(input) {
        return {
            invalid : this.state.errors[input] === undefined ? undefined: true
        }
    }

    getEmailProps() {
        let state = this.state;

        return {
            type: 'email',
            placeholder: 'Email',
            name: 'email',
            value: state.email,
            invalid : state.errors.email
        };
    }

    getPasswordProps() {
        let state = this.state;

        return {
            type: 'password',
            placeholder: 'Password',
            name: 'password',
            value: this.state.password,
            invalid : state.errors.password
        };
    }

    renderSubmitButton() {
        const submitProps = {
            color: 'info',
            block: true
        };

        return (
            <Button {...submitProps}>Iniciar</Button>
        );
    }
}

Login.propTypes = {
    loginUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);