import axios from 'axios'
import { GET_ERRORS, SET_CURRENT_USER } from './types'
import setAuthToken from '../utils/setAuthToken'
import jwt_decode from 'jwt-decode'
//Register user

export const registerUser = (userData, history) => dispatch => {
    axios.post('users/create', userData)
        .then(res => history.push('/login'))
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        })
};

//Login - get user Token

export const loginUser = (userData) => dispatch => {
    axios.post('users/login', userData)
        .then(res => {
            const { token } = res.data;
            
            localStorage.setItem('jwtToken', token);
            //set token to auth header
            setAuthToken(token);
            //decode token to get user data
            const decoded = jwt_decode(token);
            //set current user
            dispatch(setCurrentUser(decoded))

        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        })
}; 

//set logged user

export const setCurrentUser = (decoded) => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded 
    };
};

// log user out

export const logoutUser = () => dispatch => {
    //remove token for local storage
    localStorage.removeItem('jwtToken');
    // remove auth header for future request
    setAuthToken(false);
    //set current user to empty which will set isAuthenticated to false
    dispatch(setCurrentUser({}))
}