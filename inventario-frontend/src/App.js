import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken'
import { setCurrentUser, logoutUser } from './actions/authActions'

import Footer from './components/layout/Footer'
import Landing from './components/layout/Landing'
import Login from './components/auth/Login'
import Navbar from './components/layout/Navbar'
import Register from './components/auth/Register'
import { Provider } from 'react-redux';

import store from './store'
import './App.css';

//check for token
if(localStorage.jwtToken) {
  //set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // decode token and get user info
  const decoded = jwt_decode(localStorage.jwtToken);
  //set user and is authenticated
  store.dispatch(setCurrentUser(decoded))
  //check for expired token

  const currentTime = Date.now() / 1000;
  //logut user
  if(decoded.exp < currentTime) {
    store.dispatch(logoutUser())
  }
  //redirect to login
  window.location.href = '/login';
}

class App extends Component {
  render() {
    return (
      <Provider store = { store }>
        <Router>
          <div className="App">
            <Navbar />
              <Route exact path="/" component= {Landing} />
              <div className="container">
              <Route exact path="/register" component= {Register} />
              <Route exact path="/login" component= {Login} />
              </div>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
