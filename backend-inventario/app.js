const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");
const passport = require('passport')
const users = require('./components/users/user-route'); // Imports routes for the products

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));

app.use(passport.initialize());
require('./config/passport')(passport);

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use('/users', users);

module.exports = app;
