const Validator = require('validator');
const isEmpty = require('./isEmpty')

const validateLoginInput = (data) => {
    let errors = {};

    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';

    if(Validator.isEmpty(data.email)) {
        errors.email = "El campo email es requerido"
    } 

    if(Validator.isEmpty(data.password)) {
        errors.password = "El campo contraseña es requerido"
    }

    if(!Validator.isEmail(data.email)) {
        errors.email = "Email invalido"
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}

module.exports = validateLoginInput;