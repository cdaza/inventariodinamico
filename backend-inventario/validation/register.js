const Validator = require('validator');
const isEmpty = require('./isEmpty')

const validateRegisterInput = (data) => {
    let errors = {};

    data.name = !isEmpty(data.name) ? data.name : '';
    data.email = !isEmpty(data.email) ? data.email : '';
    data.password = !isEmpty(data.password) ? data.password : '';
    data.password2 = !isEmpty(data.password2) ? data.password2 : '';

    if(Validator.isEmpty(data.name)) {
        errors.name = "El campo nombre es requerido"
    } 

    if(Validator.isEmpty(data.email)) {
        errors.email = "El campo email es requerido"
    } 

    if(Validator.isEmpty(data.password)) {
        errors.password = "El campo contraseña es requerido"
    }

    if(Validator.isEmpty(data.password2)) {
        errors.password2 = "El campo confirmar contraseña es requerido"
    } 

    if(!Validator.isLength(data.name, { min: 2, max: 30})){
        errors.name = 'Nombre debe tener entre 2 y 30 caracteres'
    }

    if(!Validator.isLength(data.password, { min: 6, max: 30})){
        errors.password = 'La contraseña debe tener entre 6 y 30 caracteres'
    }

    if(!Validator.equals(data.password, data.password2)) {
        errors.password2 = 'Ambas contraseñas deben coincidir'
    }

    if(!Validator.isEmail(data.email)) {
        errors.email = "Email invalido"
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}

module.exports = validateRegisterInput;