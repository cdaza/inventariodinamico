const mongoose = require("mongoose");
const dataBaseRoute = require("./config/mongo-config-properties.js").dataBaseRoute

module.exports = {
  databaseRoute: dataBaseRoute,

  createConnection: function() {
    // connects our back end code with the database
    let dbRoute = this.databaseRoute;
    mongoose.connect(
      dbRoute,
      { useNewUrlParser: true }
    );
    this.validateConnection(mongoose.connection)
  },

  validateConnection: function(dbConnnection) {
    //validate if connection is open one time
    dbConnnection.once("open", () => console.log("connected to the database"));
    // checks if exist an error anytime
    dbConnnection.on("error", console.error.bind(console, "MongoDB connection error:"));
  }
}
