const userModel = require('./user-model');
const { parse } = require('querystring');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
const keys = require('../../config/mongo-config-properties');
const validateRegisterInput = require('../../validation/register')
const validateLoginInput = require('../../validation/login')

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.userCreate = (req, res) => {

    const { errors, isValid} = validateRegisterInput(req.body)

    if(!isValid) {
        return res.status(400).json(errors);
    }

    userModel.findOne({ email: req.body.email })
        .then(user => {
            if(user) {
                errors.email = 'El email ya se encuentra registrado';
                return res.status(400).json(errors);
            } else {
                const newUser = new userModel(
                    {
                        name: req.body.name,
                        email: req.body.email,
                        password: req.body.password
                    });
                bcrypt.genSalt(10, (error, salt) => {
                    bcrypt.hash(newUser.password, salt, (error, hash) => {
                        newUser.password = hash;
                        newUser.save()
                            .then(user => res.json(user))
                            .catch(error => console.log(error));
                    }); 
                })
            }
        })
};

exports.userLogin = (req, res) => {
    const { errors, isValid } = validateLoginInput(req.body)
    
    if(!isValid) {
        return res.status(400).json(errors);
    }

    const email = req.body.email;
    const password = req.body.password;

    userModel.findOne( {email})
        .then(user => {
            if(!user) {
                errors.email = 'Usuario no encontrado'
                return res.status(400).json(errors)
            }
            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    if(isMatch) {
                       const payload = {id: user._id, name: user.name}
                       jwt.sign(payload, keys.secretOrKey, { expiresIn: 3600}, (err, token) => {
                           res.json( {
                               success: true,
                               token: 'Bearer ' + token
                           });
                       });
                    } else {
                        errors.password = 'Contraseña incorrecta';
                        return res.status(400).json(errors)
                    }
                })
        })
};

exports.userDetails = (req, res) => {
    userModel.findById(req.params.id, (err, user) => {
        if (err) return handleError(err);
        res.send(user);
    })
};

exports.userUpdate = (req, res) => {
    userModel.findByIdAndUpdate(req.params.id, {$set: req.body}, (err, user) => {
        if (err) return handleError(err);
        res.send('User udpated.');
    });
};

exports.userDelete = function (req, res) {
    userModel.findByIdAndRemove(req.params.id, (err) => {
        if (err) return handleError(err);
        res.send('Deleted successfully!');
    })
};

exports.userGetAll = function (req, res) {
    userModel.find({}, (err, users) => {
        var userMap = {};

        users.forEach((user) => {
          userMap[user._id] = user;
        });

        res.send(userMap);
      });
};

exports.userNotFound = (req, res) => {
    res.status(404).send("Not found");
};

const handleError = (err) => {
    return err;
}
