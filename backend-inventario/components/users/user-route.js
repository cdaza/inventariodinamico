const express = require('express');
const router = express.Router();
const passport = require('passport');

// Require the controllers WHICH WE DID NOT CREATE YET!!
const userController = require('./user-controller.js');

// a simple test url to check that all of our files are communicating correctly.
router.get('/test', userController.test);
router.get('/allUsers', userController.userGetAll);

router.post('/create', userController.userCreate);

router.post('/login', userController.userLogin);

router.get('/current', passport.authenticate('jwt', { session: false }), 
    (req, res) => {
        res.json({
            id: req.user.id,
            name: req.user.name,
            email: req.user.email
        })
});

router.get('/:id', userController.userDetails);

router.put('/:id/update', userController.userUpdate);

router.delete('/:id/delete', userController.userDelete);

//The 404 Route (ALWAYS Keep this as the last route)
router.get('/', userController.userNotFound);

module.exports = router;