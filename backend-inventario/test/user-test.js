const chai = require('chai');
const chaiHttp = require('chai-http');

const app = require('../app')
const should = chai.should();
chai.use(chaiHttp);

describe("Get test", () => {
  describe("GET /", () => {
      it("should get test message", (done) => {
        chai.request(app)
        .get('/users/test')
        .end((err, res) => {
            res.should.have.status(200);
            res.text.should.to.equal('Greetings from the Test controller!');
            done();
        });
       });
  });
});

describe("Get not found", () => {
  describe("GET /", () => {
      it("should get test message", (done) => {
        chai.request(app)
        .get('/notFound')
        .end((err, res) => {
            res.should.have.status(404);
            //res.text.should.to.equal('Not found');
            done();
        });
       });
  });
});

describe("Get not found", () => {
  describe("GET /", () => {
      it("should get validation message", (done) => {
        chai.request(app)
        .post('/users/create')
        .send({ name: 'a'})
        .end((err, res) => {

            let checkObj = {
              email: 'Email invalido',
              password: 'La contraseña debe tener entre 6 y 30 caracteres' ,
              password2: 'El campo confirmar contraseña es requerido',
              name: 'Nombre debe tener entre 2 y 30 caracteres',
            } 
      
            res.should.have.status(400);
            res.should.be.json;
            //res.body.should.be.equal(checkObj);
            done();
        });
       });
  });
});
