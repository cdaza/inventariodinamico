const app = require('./app')
const dataBaseController = require("./data-base-controller.js")

const API_PORT = 3002;

dataBaseController.createConnection();

// launch our backend into a port|
app.listen(API_PORT, () => console.log(`LISTENING ON PORT ${API_PORT}`));